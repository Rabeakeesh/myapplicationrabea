package com.example.a.myapplicationrabea;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Retrieve extends Activity {

    ListView listView;
    FirebaseDatabase database;
    DatabaseReference ref;
    ArrayList<User> list;
    ArrayAdapter<User> adapter;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrieve);

        user = new User();
        listView = (ListView)findViewById(R.id.listView);
        database = FirebaseDatabase.getInstance();
        ref = database.getReference("User");
        list = new ArrayList<>();
        adapter = new ArrayAdapter<User>(this,R.layout.user_info,R.id.userInfo,list);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds:dataSnapshot.getChildren()){

                    user = ds.getValue(User.class);
                    list.add(user);
                }
                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
